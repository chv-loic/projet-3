<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link href="..\style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    </head>

    <body>

<!--ARTICLE CONTENT-->


        <section id="art-content">

            <div class="container-fluid" id="div-art">

                    <div class="row">

                         <a href="../home.html" id="back">RETOUR AU SITE</a>

                        <h1 id="title-head">Films en plein air</h1>

                        <a id="reserv" href="reserv.html">M'INSCRIRE</a>

                    </div>


                <div class="row" id="row-actu">
                                <div class="col-lg-3 col-actu">


                                    <div class="block-article">
                                        <div class="art-img">
                                            <h3>Titre article</h3>
                                            <p class="extrait-art">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>

                                            <div class="btn-art">
                                                <a href="article/art-1.html">Voir plus</a>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                                <div class="col-lg-3 col-actu">

                                   <div class="block-article">
                                    <div class="art-img">
                                            <h3>Titre article</h3>
                                            <p class="extrait-art">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>

                                            <div class="btn-art">
                                                <a href="article/art-1.html">Voir plus</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-3 col-actu">

                                    <div class="block-article">
                                    <div class="art-img">
                                            <h3>Titre article</h3>
                                            <p class="extrait-art">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>

                                            <div class="btn-art">
                                                <a href="article/art-1.html">Voir plus</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                 <div class="col-lg-3 col-actu">

                                    <div class="block-article">
                                    <div class="art-img">
                                            <h3>Titre article</h3>
                                            <p class="extrait-art">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>

                                            <div class="btn-art">
                                                <a href="article/art-1.html">Voir plus</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row" id="row-actu">
                                <div class="col-lg-3 col-actu">


                                    <div class="block-article">
                                        <div class="art-img">
                                            <h3>Titre article</h3>
                                            <p class="extrait-art">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>

                                            <div class="btn-art">
                                                <a href="article/art-1.html">Voir plus</a>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                                <div class="col-lg-3 col-actu">

                                   <div class="block-article">
                                    <div class="art-img">
                                            <h3>Titre article</h3>
                                            <p class="extrait-art">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>

                                            <div class="btn-art">
                                                <a href="article/art-1.html">Voir plus</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-3 col-actu">

                                    <div class="block-article">
                                    <div class="art-img">
                                            <h3>Titre article</h3>
                                            <p class="extrait-art">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>

                                            <div class="btn-art">
                                                <a href="article/art-1.html">Voir plus</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                 <div class="col-lg-3 col-actu">

                                    <div class="block-article">
                                    <div class="art-img">
                                            <h3>Titre article</h3>
                                            <p class="extrait-art">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>

                                            <div class="btn-art">
                                                <a href="article/art-1.html">Voir plus</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>


            </div>
        </section>

    </body>
</html>
