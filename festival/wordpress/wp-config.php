<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'festival');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u$Z* ,j-o+GCm#J2gbM6VYJid0fB=6z;^M? ;:I3xImTLgqLrg6;4(1Ehrb2O`w^');
define('SECURE_AUTH_KEY',  'Ph7km}t{!u/A3Rw3xoWk< naK  BfYyUS8YJ(:+aR-#Ur`<^8r`BzRk%cylTDZ&X');
define('LOGGED_IN_KEY',    'pTTlkSX13o8P{1NPLbqB2LFmXlIK7r``e!Sn6eN~djS-b6eX1Dp.*E<qg6tE.Yp9');
define('NONCE_KEY',        '`DI2/&/}VXO-PP]{.+W=/fXWjrxH:{lq:PHoUk5I`?ns)0kfcBn3~[MP4_x[MTNY');
define('AUTH_SALT',        'g-&pU03*;E`.ZZ6=%X-+X4;fy+a[qK5L j^0@:ur^ui}[5l` ;&)eNOS]Ic7jUFd');
define('SECURE_AUTH_SALT', 'P~77y?56P0K081O!(78f>,rh?T0TE8CzuUpkN _?/`.G-:9eXq=yxrI(Y3r#o6h;');
define('LOGGED_IN_SALT',   'lCjnxytNJ_](j#Oqzt&JlIta}78bCEmNR;@St,(_@6dPv.F0|za:M<< xbllo|3*');
define('NONCE_SALT',       'MxWtval5lA_>/Ozmaz8M^^i=%:Xcj8kV[9*-Z.hT[v2qBYjQ>+CMpRt`n.6bi3Q ');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'p3_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');